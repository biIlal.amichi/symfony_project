<?php

namespace App\extentions;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class PersonalTwigExtension extends AbstractExtension
{

    public function getFilters(): array
    {
        return [
            new TwigFilter('defaultImage',[$this, 'defaultImage'])
        ];
    }

    public function defaultImage(string $path): string
    {
        if (strlen(trim($path)) == 0) {
            return 'moi.PNG';
        }
        return $path;
    }
}