<?php

namespace App\Controller;

use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function mysql_xdevapi\getSession;

class TodoController extends AbstractController
{


    /**
     * @Route("/todo",name="app_todo")
     */
   // #[Route('/todo', name: 'app_todo')]
    public function index(Request $request): Response
    {
        $session = $request->getSession();

        if (!$session->has('todos')) {
            $todos = [
                'todo1' => 'test1',
                'todo2' => 'test 2',
                'todo3' => 'test3'
            ];
            $session->set('todos', $todos);
            $this->addFlash('info', "todo initialsé ");
        }


        return $this->render('todo/index.html.twig');
    }


    #[Route('/todo/add/{name}/{content}', name: 'todo.add')]
    public function addTodo(Request $request, $name, $content)
    {
        $session = $request->getSession();
        if ($session->has('todos')) {
            $todos = $session->get('todos');
            if (isset($todos[$name])) {

                $this->addFlash('error', "la clé $name existe déja");
            } else {
                $todos[$name] = $content;
                $session->set('todos', $todos);
                $this->addFlash('success', "todo ajouté avec succès");
            }
        } else {
            $this->addFlash('error', "todo non insialisé !");

        }
        return $this->redirectToRoute('app_todo');
    }


    #[Route('/todo/reset', name: 'todo.reset')]
    public function resetTodo(Request $request): RedirectResponse
    {
        $session = $request->getSession();
        $session->remove('todos');
        return $this->redirectToRoute('app_todo');
    }


    #[Route('/todo/update/{name}/{content}', name: 'todo.update')]
    public function updateTodo(Request $request, $name, $content): RedirectResponse
    {
        $session = $request->getSession();
        if ($session->has('todos')) {
            $todos = $session->get('todos');
            if (!isset($todos[$name])) {

                $this->addFlash('error', "la clé $name n'existe pas");
            } else {
                $todos[$name] = $content;
                $session->set('todos', $todos);
                $this->addFlash('success', "todo mise à jour avec succès");
            }
        } else {
            $this->addFlash('error', "todo non insialisé !");

        }
        return $this->redirectToRoute('app_todo');
    }

    #[Route('/todo/delete/{name}', name: 'todo.delete')]
    public function deleteTodo(Request $request, $name): RedirectResponse
    {
        $session = $request->getSession();
        if ($session->has('todos')) {
            $todos = $session->get('todos');
            if (isset($todos[$name])) {
                unset($todos[$name]);
                $this->addFlash('success', " $name supprimé");
                $session->set('todos', $todos);
            }else{
                $this->addFlash('error', "la clé $name n'existe pas");
            }
        } else {
            $this->addFlash('error', "todo non insialisé !");

        }
        return $this->redirectToRoute('app_todo');

    }


}
