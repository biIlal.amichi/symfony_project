<?php

namespace App\Controller;

use phpDocumentor\Reflection\Types\This;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FirstController extends AbstractController
{
    #[Route('/first', name: 'first')]
    public function index(): Response
    {
        return $this->render('first/index.html.twig', [
            'name' => "amichi",
            'firstname' => "bilal"
        ]);
    }


    #[Route ('/sayHello', name: 'say_hello')]
    public function sayHello(): Response
    {
        $rand = rand(1, 10);
        echo "rand = ", $rand;
        if ($rand > 5) {
            return $this->redirectToRoute("first");
        }
        return $this->render('first/sayHello.html.twig', ['name' => 'bil', 'path'=>'   ']);

    }

    #[Route('/forwardPage', name: 'Forward_page')]
    public function  forwardPage (): Response {
        echo "forward to index";
        return $this->forward('App\Controller\FirstController::index');

}


#[Route('/template', name:'template')]
public function template( )
{
    return $this->render('template.html.twig');
}
}
